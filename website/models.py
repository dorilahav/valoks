"""This module contains all the models for the website."""

from datetime import datetime
from uuid import uuid4 as uuid
from mongoengine import (Document, UUIDField, StringField, DateTimeField, LongField, ListField, EmbeddedDocument,
                         FloatField, SequenceField, EmbeddedDocumentListField, connect as mongo_connect, disconnect)
import config


def connect():
    database = config.mongo['database']
    host = config.mongo['host']
    port = config.mongo['port']
    username = config.mongo['username']
    password = config.mongo['password']

    mongo_connect(database, host=host, port=port, username=username, password=password)


class User(Document):
    user_id = UUIDField(primary_key=True, default=uuid)
    discord_id = LongField(required=True, unique=True)
    username = StringField(required=True)
    creation_date = DateTimeField(required=True, default=datetime.utcnow)

    meta = {
        'collection': config.mongo_collections['users'],
        'indexes': ['discord_id']
    }


class UsersManager:

    @staticmethod
    def _find_one(**kwargs):
        try:
            connect()
            return User.objects.get(**kwargs)
        finally:
            disconnect()

    @staticmethod
    def save(user: User):
        try:
            connect()
            user.save()
        finally:
            disconnect()

    @staticmethod
    def find_by_discord_id(discord_id):
        return UsersManager._find_one(discord_id=discord_id)

    @staticmethod
    def find_by_user_id(user_id):
        return UsersManager._find_one(user_id=user_id)


class Plan(EmbeddedDocument):
    plan_id = SequenceField(primary_key=True, collection_name='counters')
    name = StringField(required=True)
    price = FloatField(required=True)
    giveaway_amount = FloatField(required=True)
    duration = LongField(required=True)


class Package(Document):
    package_id = SequenceField(primary_key=True, collection_name='counters')
    name = StringField(required=True)
    icon = StringField(required=True)
    display_name = StringField(required=True)
    description = ListField(StringField(), required=True)
    plans = EmbeddedDocumentListField(Plan, required=True)

    def add_plan(self, plan: Plan):
        self.plans.append(plan)

    meta = {
        'collection': config.mongo_collections['packages']
    }


class PackagesManager:
    packages = []

    @staticmethod
    def save(package: Package):
        try:
            connect()
            package.save()
        finally:
            disconnect()

    @classmethod
    def load(cls):
        try:
            connect()
            for package in Package.objects():
                cls.packages.append(package)
        finally:
            disconnect()

    @staticmethod
    def plans_by_package_id(package_id):
        try:
            connect()
            return Package.objects(package_id=package_id).only('plans').get().plans
        finally:
            disconnect()
