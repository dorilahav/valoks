"""This module is the main app module from which the app is run."""

import os
from flask import Flask
from pymongo import MongoClient
from flask_discord import configs
import config
import controllers
from sessions import Session
from models import PackagesManager


app = Flask(__name__, static_url_path='/')


@app.context_processor
def context():
    return {'title': 'Valoks', 'icon': '/images/favicon.png'}


def _initialize_data():
    database = config.mongo['database']
    collections = config.mongo_collections
    host = config.mongo['host']
    port = config.mongo['port']
    username = config.mongo['username']
    password = config.mongo['password']

    app.config['SESSION_TYPE'] = 'mongodb'
    app.config['SESSION_MONGODB_DB'] = database
    app.config['SESSION_MONGODB_COLLECT'] = collections['sessions']
    app.config['SESSION_MONGODB'] = MongoClient(
        connect=False,
        host=host,
        port=port,
        username=username,
        password=password
    )

    session = Session()
    session.init_app(app)

    PackagesManager.load()


def _register_blueprints():
    app.register_blueprint(controllers.home)
    app.register_blueprint(controllers.users, url_prefix='/users')
    app.register_blueprint(controllers.control_panel, url_prefix='/admin')


def _init():
    address = config.website['address']

    app.config['DISCORD_CLIENT_ID'] = config.auth['client_id']
    app.config['DISCORD_CLIENT_SECRET'] = config.auth['client_secret']
    app.config['DISCORD_REDIRECT_URI'] = f'{address}/users/complete_login'
    configs.DEFAULT_SCOPES = config.auth['scope']
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    _register_blueprints()
    _initialize_data()


_init()
