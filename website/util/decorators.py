from functools import wraps
from auth import optional_logged_in_user
from response import redirect_to_login
from inspect import signature


def _function_parameters(function):
    return dict(signature(function).parameters)


def require_login(function):
    @wraps(function)
    def wrapper(*args, **kwargs):
        user = optional_logged_in_user()

        if not user:
            return redirect_to_login()

        parameters = _function_parameters(function)
        if 'user' in parameters:
            kwargs['user'] = user

        return function(*args, **kwargs)

    return wrapper
