"""This module contains all the response functions, used in the controllers to
send responses in the right format."""

from flask import jsonify, redirect, url_for, session, request

_RETURN_URL_KEY = 'return_url'


def _response(status, **content):
    return jsonify(content), status


def success(status=200, **content):
    return _response(status, **content)


def _error(message, status=400, **content):
    response = content.copy()
    response['message'] = message
    return _response(status, **response)


def already_logged_in():
    return _error('Already logged in!', status=309)


def redirect_to_login():
    session[_RETURN_URL_KEY] = request.url
    return redirect(url_for('home.login'))


def redirect_to_index():
    return redirect(url_for('home.index'))


def redirect_to_return_url():
    return_url = session.pop(_RETURN_URL_KEY, None)
    if not return_url:
        return redirect_to_index()
    return redirect(return_url)
