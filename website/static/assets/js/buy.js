$("#purchase").submit(function (event) {

    event.preventDefault();

    const form = $(this);
    const data = getFormData(form);
    const server_id = data.server_id;
    delete data['server_id'];

    Object.keys(data).forEach(function(key) {
        const value = data[key];

        if (value === 'on') {
            data[key] = true;
        }
    });

    console.log(data);

    $.ajax({
        type: 'POST',
        url: `/api/purchase/${server_id}`,
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function (data) {
            console.log(data);
        },
        error: function(error) {
            console.log(error);
        }
    });

});