"""This module is in charge of loading and initializing the config file."""

from os import path
from yaml import load, dump, Loader, Dumper

_FILE_NAME = 'config.yml'
_config = None
_DEFAULT_CONFIG = '''
website:
    name: valoks
    address: http://localhost:5000

mongo:
    host: localhost
    port: 27017
    database: valoks
    username: null
    password: null
    collections:
        sessions: sessions
        users: users
        packages: packages

auth:
    client_id: 645299331854893097
    client_secret: YCj9smF2Tco6AcneOjjl4vJbVEB7bmZX
    scope:
    - identify
    - email
    - guilds
'''


def initialize():
    global _config
    _config = load(_DEFAULT_CONFIG, Loader=Loader)

    file_path = path.join(path.curdir, _FILE_NAME)
    if path.isfile(file_path):
        with open(file_path, 'r') as config_file:
            _config.update(load(config_file, Loader=Loader))
    else:
        with open(file_path, 'w+') as config_file:
            dump(_config, config_file, Dumper=Dumper, default_flow_style=False, sort_keys=False)


def packages():
    file_name = 'packages.yml'
    default = '''
    - name: "dexter"
      icon: "/images/dexter.png"
      display_name: "Dexter's"
      description:
        - "150-200 Joins on Average"
        - "$5 - $15 Giveaway (must join your server to win)"
        - "x2 @everyone tags"
        - "Custom message in #advertisement"
      plans:
        - name: "$20 Plan ---> $5 giveaway (3 Days) + x1 @everyone"
          price: 20
          giveaway_amount: 5
          duration: 4320
        - name: "$30 Plan ---> $10 giveaway (3 Days) + x1 @everyone"
          price: 30
          giveaway_amount: 10
          duration: 4320
        - name: "40 Plan ---> $15 giveaway (3 Days) + x1 @everyone"
          price: 40
          giveaway_amount: 15
          duration: 4320
    
    - name: "dexter"
      icon: "/images/dexter.png"
      display_name: "Dexter's"
      description:
        - "150-200 Joins on Average"
        - "$5 - $15 Giveaway (must join your server to win)"
        - "x2 @everyone tags"
        - "Custom message in #advertisement"
      plans:
        - name: "$20 Plan ---> $5 giveaway (3 Days) + x1 @everyone"
          price: 20
          giveaway_amount: 5
          duration: 4320
        - name: "$30 Plan ---> $10 giveaway (3 Days) + x1 @everyone"
          price: 30
          giveaway_amount: 10
          duration: 4320
        - name: "40 Plan ---> $15 giveaway (3 Days) + x1 @everyone"
          price: 40
          giveaway_amount: 15
          duration: 4320
    
    - name: "dexter"
      icon: "/images/dexter.png"
      display_name: "Dexter's"
      description:
        - "150-200 Joins on Average"
        - "$5 - $15 Giveaway (must join your server to win)"
        - "x2 @everyone tags"
        - "Custom message in #advertisement"
      plans:
        - name: "$20 Plan ---> $5 giveaway (3 Days) + x1 @everyone"
          price: 20
          giveaway_amount: 5
          duration: 4320
        - name: "$30 Plan ---> $10 giveaway (3 Days) + x1 @everyone"
          price: 30
          giveaway_amount: 10
          duration: 4320
        - name: "40 Plan ---> $15 giveaway (3 Days) + x1 @everyone"
          price: 40
          giveaway_amount: 15
          duration: 4320
        '''

    file_path = path.join(path.curdir, file_name)
    if not path.isfile(file_path):
        with open(file_path, 'w+') as config_file:
            dump(load(default, Loader=Loader), config_file, Dumper=Dumper, default_flow_style=False, sort_keys=False)
    else:
        with open(file_path, 'r') as config_file:
            return load(config_file, Loader=Loader)


initialize()

website = _config['website']
mongo = _config['mongo']
mongo_collections = mongo['collections']
auth = _config['auth']
