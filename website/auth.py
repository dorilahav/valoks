"""This module contains all auth management related functions."""

from flask import session, current_app
from flask_discord import DiscordOAuth2Session
from models import UsersManager, User
from mongoengine import DoesNotExist

_ID_FIELD = 'user_id'
_DEFAULT_SCOPE = ['identify', 'email', 'guilds']
_ADMIN_PERMISSION = 0x8


def _auth_session():
    return DiscordOAuth2Session(current_app)


def create_auth_session(scope: list = None):
    auth_session = _auth_session()
    return auth_session.create_session(scope or _DEFAULT_SCOPE)


def _complete_verification():
    auth_session = _auth_session()
    auth_session.callback()

    return auth_session.fetch_user()


def logged_in_user():
    user_id = session.get(_ID_FIELD, None)

    if not user_id:
        raise DoesNotExist('No user id found')

    return UsersManager.find_by_user_id(user_id)


def optional_logged_in_user():
    try:
        return logged_in_user()
    except DoesNotExist:
        return None


def is_logged_in():
    try:
        logged_in_user()
        return True
    except DoesNotExist:
        return False


def login():
    discord_user = _complete_verification()
    discord_user_id = discord_user.id
    discord_username = discord_user.username

    try:
        user = UsersManager.find_by_discord_id(discord_user_id)

        if user.username != discord_username:
            user.username = discord_username
            UsersManager.save(user)

    except DoesNotExist:
        user = User(discord_id=discord_user_id, username=discord_username)
        UsersManager.save(user)

    session[_ID_FIELD] = user.user_id
    return user


def logout():
    session.pop(_ID_FIELD)


def admin_guilds():
    auth_session = _auth_session()
    guilds = []

    for guild in auth_session.fetch_guilds():
        guild = guild.__dict__['_payload']
        if (guild['permissions'] & _ADMIN_PERMISSION) == _ADMIN_PERMISSION or guild.get('owner', False):
            guilds.append({k: guild[k] for k in ['id', 'name']})

    return guilds
