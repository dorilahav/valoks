from mongoengine import get_db
from config import packages
from models import Package, PackagesManager, Plan, connect, disconnect


def drop_database():
    try:
        connect()
        database = get_db()
        database._Database__client.drop_database(database._Database__name)
    finally:
        disconnect()


def _validate_field(data, field_name, types, error_message):
    if field_name not in data or not isinstance(data[field_name], types):
        raise ValueError(error_message)


def setup_packages():
    loaded_packages = packages()
    if not loaded_packages:
        print('packages.yml created, please fill the file with your packages and run the script again!')
    else:
        for package_data in loaded_packages:
            if 'name' not in package_data or not isinstance(package_data['name'], str):
                raise ValueError('name in package is missing or not from type string')

            print(f'Creating package {package_data["name"]}...')

            _validate_field(package_data, 'icon', str, 'icon in package is missing or not from type string')
            _validate_field(package_data, 'display_name', str,
                            'display_name in package is missing or not from type string')
            _validate_field(package_data, 'description', (str, list),
                            'description in package is missing or not from type string or list')

            if 'plans' not in package_data or not isinstance(package_data['plans'], list)\
                    or not len(package_data['plans']):
                raise ValueError('plans in package is missing or not from type list or has no items in it')

            plans = []
            for plan_data in package_data['plans']:
                _validate_field(plan_data, 'name', str, 'name in plan is missing or not from type string')
                _validate_field(plan_data, 'price', (float, int), 'price in plan is missing or not from type number')
                _validate_field(plan_data, 'giveaway_amount', (float, int),
                                'giveaway_amount in plan is missing or not from type number')
                _validate_field(plan_data, 'duration', int,
                                'duration in plan is missing or not from type long. Make sure its in seconds!')

                plans.append(Plan(name=plan_data['name'],
                                  price=plan_data['price'],
                                  giveaway_amount=plan_data['giveaway_amount'],
                                  duration=plan_data['duration']))

            description = package_data['description']
            if isinstance(description, str):
                description = description.split('\n')

            package = Package(name=package_data['name'],
                              icon=package_data['icon'],
                              display_name=package_data['display_name'],
                              description=description,
                              plans=plans)

            PackagesManager.save(package)
            print(f'Package {package.name} created!')
        return True


def setup():
    print('Dropping database...')
    drop_database()
    print('Database dropped!')
    print('Setting up packages...')
    if setup_packages():
        print('Packages setup!')


if __name__ == '__main__':
    setup()
