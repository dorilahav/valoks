"""This module to manage all the website controllers."""

from .users import users
from .home import home
from .control_panel import control_panel
