"""This module is the website home controller."""

from flask import Blueprint, render_template
import auth
import response
from util import require_login
from models import PackagesManager


home = Blueprint('home', __name__)


@home.route('/index', methods=['GET'])
@home.route('/', methods=['GET'])
def index():
    return render_template('home/index.html', user=auth.optional_logged_in_user())


@home.route('/advertise', methods=['GET'])
@require_login
def advertise(user):
    return render_template('home/advertise.html', user=user, packages=PackagesManager.packages)


@home.route('/purchase/<int:package_id>', methods=['GET'])
@require_login
def purchase(package_id):
    return render_template('home/purchase.html',
                           package_id=package_id,
                           plans=PackagesManager.plans_by_package_id(package_id),
                           guilds=auth.admin_guilds())


@home.route('/login', methods=['GET'])
def login():
    if auth.is_logged_in():
        return response.redirect_to_return_url()

    return auth.create_auth_session()


@home.route('/logout', methods=['GET'])
def logout():
    if auth.is_logged_in():
        auth.logout()
    return response.redirect_to_index()
