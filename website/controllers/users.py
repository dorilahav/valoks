"""This module is the website users controller."""

from flask import Blueprint
import auth
import response

users = Blueprint('users', __name__)


@users.route('/complete_login', methods=['GET'])
def complete_login():
    auth.login()
    return response.redirect_to_return_url()


@users.route('/me', methods=['GET'])
def details():
    return "Not logged in"


@users.route('/<string:user_id>', methods=['GET'])
def user(user_id):
    return "User not found"


@users.route('/<string:user_id>', methods=['DELETE'])
def delete(user_id):
    return "User not found"
