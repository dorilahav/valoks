from flask import Blueprint, render_template

control_panel = Blueprint('control_panel', __name__)


@control_panel.route('/')
@control_panel.route('/dashboard')
def dashboard():
    return render_template('admin/dashboard.html')
